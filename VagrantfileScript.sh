DEVENV_RELEASE=${1}
ANSIBLE_DIR=/etc/ansible/upgrading
ANSIBLE_VERSION=2.2.1.0

sed -i 's/NM_CONTROLLED=no/NM_CONTROLLED=yes/g' /etc/sysconfig/network-scripts/ifcfg-eth0 # Makes NetworkManager no longer show as disconnected
systemctl restart NetworkManager

curl -o /etc/yum.repos.d/CentOS-Base.repo \
		 https://artifactory01.esss.lu.se/artifactory/list/devenv/devenv-releases/${DEVENV_RELEASE}/repofiles/CentOS-Base.repo
curl -o /etc/yum.repos.d/epel.repo \
		 https://artifactory01.esss.lu.se/artifactory/list/devenv/devenv-releases/${DEVENV_RELEASE}/repofiles/epel-19012016.repo
rpm -v --import https://packages.elastic.co/GPG-KEY-elasticsearch
rpm -v --import https://artifactory01.esss.lu.se/artifactory/list/devenv/repositories/repofiles/RPM-GPG-KEY-EPEL-7
rpm -v --import https://artifactory01.esss.lu.se/artifactory/list/devenv/repositories/repofiles/RPM-GPG-KEY-ess_ics
yum clean all
#yum downgrade -y gcc perl cpp gcc-gfortran libgfortran libquadmath libstdc++ perl-libs gcc-c++ libgcc libgomp libquadmath-devel libstdc++-devel make

curl --fail --retry 2 -o /usr/local/bin/ansible-playbook https://artifactory01.esss.lu.se/artifactory/ansible-provision/ansible-releases/${ANSIBLE_VERSION}/ansible-playbook
  [ "$?" != "0" ] && { echo "FATAL ERROR: Unable to download the ansible repository. Please try again later."; exit 1; }
chmod +x /usr/local/bin/ansible-playbook
yum install -y git

if [[ "$DEVENV_LOCAL" == "true" ]]
then
  echo "Using local ics-ans-devenv repository"
  cd /vagrant
else
  echo "Using ics-ans-devenv-${DEVENV_RELEASE} from artifactory"
  cd ${ANSIBLE_DIR}
  rm -rf ${ANSIBLE_DIR}/ics-ans-devenv
  curl --fail --retry 2 -O https://artifactory01.esss.lu.se/artifactory/list/devenv/devenv-releases/${DEVENV_RELEASE}/tarball/ics-ans-devenv-${DEVENV_RELEASE}.tar.gz
  [ "$?" != "0" ] && { echo "FATAL ERROR: Unable to download the ansible repository. Please try again later."; exit 1; }
  tar xfz ics-ans-devenv-${DEVENV_RELEASE}.tar.gz
  rm ics-ans-devenv-${DEVENV_RELEASE}.tar.gz
  cd ics-ans-devenv
fi

export PYTHONUNBUFFERED=1
export ANSIBLE_FORCE_COLOR=1
echo ansible-playbook -i \"localhost,\" -c local --extra-vars \"DEVENV_VERSION=$1 DEVENV_SSSD=$2 DEVENV_EEE=$3 DEVENV_NFSSERVER=$4 DEVENV_CSS=$5 DEVENV_OPENXAL=$6 DEVENV_IPYTHON=$7\" devenv.yml
ansible-playbook -i "localhost," -c local --extra-vars "DEVENV_VERSION=$1 DEVENV_SSSD=$2 DEVENV_EEE=$3 DEVENV_NFSSERVER=$4 DEVENV_CSS=$5 DEVENV_OPENXAL=$6 DEVENV_IPYTHON=$7" devenv.yml
