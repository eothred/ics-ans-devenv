#!/usr/bin/env bash

# Based on
# https://raymii.org/s/tutorials/Ansible_-_Playbook_Testing.html

set -o errexit
set -o nounset
# set -o xtrace
set -o pipefail

__DIR__="$(cd "$(dirname "${0}")"; echo $(pwd))"
__BASE__="$(basename "${0}")"
__FILE__="${__DIR__}/${__BASE__}"

cd ${__DIR__}/../test/

echo "################################"
echo "Build Information"
echo "Directory: $(pwd)"
echo "Filename: ${__FILE__}"
echo "Version Information:"
echo "Ansible Version: $(ansible --version)"
echo "Ansible Playbook Version: $(ansible-playbook --version)"
[ -f /etc/os-release ] && echo "Operating System: $(source /etc/os-release; echo $NAME $VERSION)"
if [[ ${OSTYPE} != darwin* ]]
then
    echo "Kernel: $(uname --kernel-name --kernel-release --processor)"
fi
echo "################################"

echo "### Starting tests"

# Current directory is test/
set +e
#find ../ -maxdepth 1 -name '*.yml'| xargs -n1 ansible-playbook --syntax-check --list-tasks -i ${__DIR__}/../inventories/syntax_check
find ../ -maxdepth 1 -name '*.yml'| xargs -n1 ansible-playbook --syntax-check --list-tasks -i ${__DIR__}/hosts-77
exit_status=$?
set -e

if [[ $exit_status == 0 ]]
then
  echo "Syntax check OK"
else
  echo "Syntax check NOT OK"
  exit $exit_status
fi
